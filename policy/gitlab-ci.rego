package main

deny[msg] {
  # access the build.image section of a GitLab Pipeline file
  x:= input[_]["image"].name
  # get the boolean value of the string of the image to see it is http
  b := endswith(x, "latest")
  # if it ends with latest, we don't know what version we ran against for compliance
  b == true 
  # print a failure message with details from above
  msg := sprintf("Need to specify version of image, latest is not accepted: %q", [x])
}
